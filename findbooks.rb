# encoding: UTF-8

# require 'rubygems' 
# require 'bundler/setup'  

# require your gems as usual 

require 'nokogiri'
require 'open-uri'
require 'pry'
require 'rest_client'
require 'json'

class FindbooksPlugin
	attr_accessor :site
end

class Books < FindbooksPlugin
	attr_accessor :url, :doc

	def initialize(isbn)
		@site = "博客來"
		@url = "http://search.books.com.tw/exep/prod_search.php?key=#{isbn}&cat=BKA"
		
		@doc = Nokogiri::HTML(open(@url))
	end

	def name
		@doc.css('h3 a[rel=mid_name]').first.text
	end

	def price
		@doc.css('span[class=price] > strong > b[2]').text
	end

	def link
		@doc.css('h3 a[rel=mid_name]').first['href']
	end

	def image
		# binding.pry
		@doc.css('a[rel=mid_image] img[class=itemcov]').first['src']
	end
end

class Taaze < FindbooksPlugin
	attr_accessor :url, :doc, :json

	def initialize(isbn)
		@site = "讀冊"
		@url = "http://www.taaze.tw/beta/searchAgent.jsp?keyword=#{isbn}&sorttype=0&resp_num=10"
		@json = JSON.parse( RestClient.get(@url) )
		# @doc = Nokogiri::HTML(open(@url))
	end

	def name
		# @json.last["RESULT1"].first["item"].first["titleMain"]
	end

	def price
		@json.last["RESULT1"].first["item"].first["salePrice"]
	end

	def link
		prodId = @json.last["RESULT1"].first["item"].first["prodId"]
		"http://www.taaze.tw/sing.html?pid=#{prodId}"
	end
end

class IRead < FindbooksPlugin
	attr_accessor :url, :doc

	def initialize(isbn)
		@site = "灰熊"
		@url = "https://www.iread.com.tw/search_results.aspx?Condition=0&skeyword=#{isbn}&SearchField=AllField"
		response = Net::HTTP.get_response(URI(@url))
		@url = "https://www.iread.com.tw/#{response['location']}"
		@doc = Nokogiri::HTML(open(@url))
	end

	def name
		# @doc.css('div#book_info h1').first.text
	end

	def price
		@doc.css('li[class=PP] div span[class=redword2]').last.text
	end

	def link
		@url
	end
end


class Cite < FindbooksPlugin
	attr_accessor :url, :doc

	def initialize(isbn)
		@site = "城邦"
		@url = "http://www.cite.com.tw/search_result.php?s_type=0&keywords=#{isbn}"
		
		@doc = Nokogiri::HTML(open(@url))
	end

	def name
		# @doc.css('div[class=book-info-1] h2 a').first.text
	end

	def price
		@doc.css('div[class=book-info-2] span[class=font-color01]').last.text
	end

	def link
		@doc.css('div[class=book-info-1] h2 a').first['href']
	end
end

# doc = Nokogiri::HTML(open('http://search.books.com.tw/exep/prod_search.php?key=9789865740375&cat=BKA'))

# [Books, Taaze, IRead, Cite].each do |klass|
# 	b = klass.new('9789865740375')
# 	puts "***************************************"
# 	begin
# 		puts "#{b.site}\n#{b.name}\nNT. $#{b.price}\n#{b.link}"
# 	rescue Exception => e
# 		puts "#{b.site}\n找不到書籍！"
# 	end
	
# 	puts "***************************************"
# end

# puts "***************************************"
# b = Cite.new('9789862724842')
# binding.pry
# puts "#{b.name}\nNT. $#{b.price}\n#{b.link}"
# puts "***************************************"

