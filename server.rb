# encoding: UTF-8

require 'rubygems' 
require 'bundler/setup'  

# require your gems as usual 

require './findbooks'

require "sinatra/base"
require "sinatra/reloader"
require "sinatra/subdomain"

require 'rack/request'
module Rack
  class Request
    def subdomains(tld_len=1) # we set tld_len to 1, use 2 for co.uk or similar
      # cache the result so we only compute it once.
      # binding.pry
      @env['rack.env.subdomains'] ||= lambda {
        # check if the current host is an IP address, if so return an empty array
        return [] if (host.nil? ||
                      /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/.match(host))
        host.split('.')[0...(1 - tld_len - 2)] # pull everything except the TLD
      }.call
      # @env['rack.env.subdomains'] = host
    end
  end
end

class MyApp < Sinatra::Base
	register Sinatra::Subdomain

  	configure :development do    
  		register Sinatra::Reloader  
  	end

  # 	get '/' do
		# @request.subdomains.join("<br/>")
		# binding.pry
  # 	end

  	subdomain :findbooks do

		get '/' do
			@keyword = params[:keyword]
			@book_sites = []

			if @keyword.nil? || @keyword.empty?
				@keyword = ""
			else
				[Books, Taaze, IRead, Cite].each do |klass|
					
					begin
						b = klass.new(@keyword)
						b.link
						@book_sites << b
					rescue Exception => e
						puts "#{b.site}\n找不到書籍！"
					end
				end
			end

			book = @book_sites.first

			@book_name = book.name
			@book_image = book.image
			puts "@book_image = #{@book_image}"
			# binding.pry

		  	erb :findbooks_index
		end
	end


	subdomain :foo do
	  get '/' do
	    "What does #{subdomain} rhyme with? Oh, you!"
	  end
	end

	subdomain do
	  # Route for the root
	  get '/' do
	    "You're on the #{subdomain} home page! Good job!"
	  end
	  # Route for a different page
	  get '/bananas' do
	    "You're #{subdomain} for bananas! Good job!"
	  end
	end

	get '/' do
  		erb :index
	end
end